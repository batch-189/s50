import {Row, Col, Card, Button} from 'react-bootstrap'

export default function SampleCourse(){

	return (

		<Row className="mt-2 mb-2">
			<Col xs={12} md={4}>
				<Card className="SampleCourse p-3">
				 <Card.Body>
      				  <Card.Title>Sample Course</Card.Title>
      				  
      				  <Card.Text>
      				  	<h6>Description:</h6>
          				<p>This is sample course offering.</p>
          				<h6>Price:</h6>
          				<p>Php 40,000</p>
       				 </Card.Text>
       				 <Button variant="primary">Enroll Now!</Button>
      				</Card.Body>
   				 </Card>
			</Col>
		</Row>

		)
}